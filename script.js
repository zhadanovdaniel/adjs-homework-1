class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  getName() {
    return this.name
  }
  setName(name){
    this.name = name;
  }

  getAge() {
    return this.age
  }
  setAge(age){
    this.age = age;
  }

  getSalary() {
    return this.salary
  }
  setSalary(salary){
    this.salary = salary;
  }

}
class Programmer extends Employee{
    constructor(name, age, salary, lang){
       super(name, age, salary)
        this.lang = lang; 
       
    }
    getSalary(){
        return this.salary * 3;
    }
    
}
const user = new Employee("daniel", 19, 2000)
console.log('user.get() :>> ', user.getName());

const programmer1 = new Programmer("Van", 46, 300, "eng")
console.log('programmer1 :>> ', programmer1);
const programmer2 = new Programmer("Billy", 54, 600, "germ")
console.log('programmer2 :>> ', programmer2);
const programmer3 = new Programmer("Tom", 93, 5000, "typeScript")
console.log('programmer3 :>> ', programmer3);